package com.orene.orenekaraoke;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Helpers.BaseActivity;
import com.orene.orenekaraoke.Helpers.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OrepayActivity extends BaseActivity {
    private Toolbar toolBar;
    EditText etOrepay;
    Button btnSend;
    Map<String, String> user;
    private TextView tvLangkah;
    private LinearLayout layoutLangkah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orepay);

        toolBar = findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Topup");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etOrepay = findViewById(R.id.et_orepay);
        btnSend = findViewById(R.id.btn_send);
        user = sesi.getUserDetails();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validasi(etOrepay)) {
                    sendOrder();
                }
            }
        });
        tvLangkah = findViewById(R.id.tvLangkah);
        layoutLangkah = findViewById(R.id.layoutLangkah);
        layoutLangkah.setVisibility(View.GONE);
    }

    public boolean validasi(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        View focusview = null;

        if (Integer.parseInt(text) < 10000) {
            editText.setError(Html
                    .fromHtml("<font color='red'>Nominal Minimal Rp.10.000</font>"));
            focusview = editText;
            focusview.requestFocus();
            return false;
        }

        return true;
    }

    private void sendOrder() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Sending Order...");
        HashMap<String, String> params = new HashMap<>();
        params.put("top_up", etOrepay.getText().toString());
        params.put("email", user.get(SessionManager.KEY_EMAIL));
        aq.progress(dialog).ajax(Config.BASE_URL + "home/topup_orepay", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        Toast.makeText(OrepayActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        if (object.getString("success").equals("1")) {
                            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            layoutLangkah.setVisibility(View.VISIBLE);
                            tvLangkah.setText(Html.fromHtml("Silahkan transfer sejumlah<br><br>Rp.<b>" + etOrepay.getText().toString() + "</b><br><br>Ke rekening BCA dengan nomor rekening<br><br><b>222555665</b>" +
                                    "<br><br>Jika Anda sudah transfer, maka saldo akan segera diproses<br>Terima Kasih."));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (status.getCode() == 404) {
                        Toast.makeText(OrepayActivity.this, "Resource not found!", Toast.LENGTH_SHORT).show();
                    } else if (status.getCode() == 500) {
                        Toast.makeText(OrepayActivity.this, "Server is busy or down!", Toast.LENGTH_SHORT).show();
                    } else if (status.getCode() == -101) {
                        Toast.makeText(OrepayActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrepayActivity.this, "Unexpected error occured", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
