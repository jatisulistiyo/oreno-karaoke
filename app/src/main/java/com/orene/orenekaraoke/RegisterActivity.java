package com.orene.orenekaraoke;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Helpers.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jati on 10/22/2017.
 */

public class RegisterActivity extends BaseActivity {
    EditText etNama,etEmail,etCp,etAlamat,etTTL,etPass;
    Button btnRegister;
    Spinner sp_sex;
    String strsex;
    int mYear,mMonth,mDay;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etNama = findViewById(R.id.et_nama);
        etEmail = findViewById(R.id.et_email);
        etCp = findViewById(R.id.et_cp);
        etAlamat = findViewById(R.id.et_alamat);
        sp_sex = findViewById(R.id.sp_sex);
        etTTL = findViewById(R.id.et_ttl);
        etPass = findViewById(R.id.et_pass);
        btnRegister = findViewById(R.id.btn_regist);
        etTTL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                etTTL.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        sp_sex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                strsex = String.valueOf(adapterView.getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                strsex = String.valueOf(adapterView.getSelectedItem());
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validasi(etNama)&&validasiEmail(etEmail)&&validasi(etCp)&&validasi(etAlamat)&&validasi(etTTL)&&validasi(etPass)){
                    register();
                }
            }
        });
    }
    public boolean validasiEmail(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        View focusview = null;

        if (!Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
            editText.setError(Html
                    .fromHtml("<font color='red'>Format email tidak sesuai (xxxx@yyy.zzz)</font>"));
            focusview = editText;
            focusview.requestFocus();
            return false;
        }

        return true;
    }
    public boolean validasi(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        View focusview = null;

        if (text.length() == 0) {
            editText.setError(Html
                    .fromHtml("<font color='red'>Tidak boleh kosong</font>"));
            focusview = editText;
            focusview.requestFocus();
            return false;
        }

        return true;
    }
    private void register(){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Registering...");
        Map<String,String> params = new HashMap<String,String>();
        params.put("nama",etNama.getText().toString());
        params.put("email",etEmail.getText().toString());
        params.put("cp",etCp.getText().toString());
        params.put("alamat",etAlamat.getText().toString());
        params.put("sex",strsex);
        params.put("password",etPass.getText().toString());
        aq.progress(dialog).ajax(Config.BASE_URL + "home/register_user",params,JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object!=null) {
                    try {
                        Toast.makeText(RegisterActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        if (object.getString("success").equals("1")) {
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    if (status.getCode()==-101){
                        Toast.makeText(c,"No internet connection",Toast.LENGTH_LONG).show();
                    }else if(status.getCode()==404){
                        Toast.makeText(c, "Resource not found", Toast.LENGTH_SHORT).show();
                    }else if (status.getCode()==500){
                        Toast.makeText(c, "Server Down", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}

