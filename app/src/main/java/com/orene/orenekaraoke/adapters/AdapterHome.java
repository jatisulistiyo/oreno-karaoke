package com.orene.orenekaraoke.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orene.orenekaraoke.ActivityInputData;
import com.orene.orenekaraoke.Config;
import com.orene.orenekaraoke.R;
import com.orene.orenekaraoke.models.ItemHome;

import java.util.List;

/**
 * Created by jati on 9/9/17.
 */

public class AdapterHome extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemHome> listHome;
    private Context context;

    public AdapterHome(List<ItemHome> listHome, Context context) {
        this.listHome = listHome;
        this.context = context;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvAddress;
        private ImageView ivThumbnails;
        private LinearLayout containerItemHome;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            ivThumbnails = itemView.findViewById(R.id.ivThumbnails);
            containerItemHome = itemView.findViewById(R.id.containerItemHome);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        final ItemHome current = listHome.get(position);
        viewHolder.tvName.setText(current.strName);
        viewHolder.tvAddress.setText(current.strAddress);
        Glide.with(context).load(Config.BASE_IMG_PLACE+current.thumbnails).into(viewHolder.ivThumbnails);
        viewHolder.containerItemHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ActivityInputData.class);
                intent.putExtra("id_place",current.idPlace);
                intent.putExtra("name_place",current.strName);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listHome.size();
    }
}
