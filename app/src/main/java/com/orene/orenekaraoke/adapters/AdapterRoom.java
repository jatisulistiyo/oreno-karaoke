package com.orene.orenekaraoke.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.orene.orenekaraoke.OrderFinishActivity;
import com.orene.orenekaraoke.R;
import com.orene.orenekaraoke.models.ItemRoom;

import java.util.List;

/**
 * Created by jati on 9/9/17.
 */

public class AdapterRoom extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemRoom> listRoom;
    private Context context;

    public AdapterRoom(List<ItemRoom> listRoom, Context context) {
        this.listRoom = listRoom;
        this.context = context;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvNumber;
        private RelativeLayout containerItemInputRoom;

        public ViewHolder(View itemView) {
            super(itemView);
            tvNumber = itemView.findViewById(R.id.tvNumber);
            containerItemInputRoom = itemView.findViewById(R.id.containerItemInputRoom);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inputdata_room, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemRoom itemRoom = listRoom.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.tvNumber.setText(itemRoom.strNumber);
        if (itemRoom.status.equals("Unavailable")){
            viewHolder.containerItemInputRoom.setBackgroundColor(Color.RED);
            viewHolder.tvNumber.setTextColor(Color.WHITE);
        }
    }

    @Override
    public int getItemCount() {
        return listRoom.size();
    }
}
