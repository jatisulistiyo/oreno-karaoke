package com.orene.orenekaraoke.adapters;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.orene.orenekaraoke.ActivityInputData;
import com.orene.orenekaraoke.Config;
import com.orene.orenekaraoke.R;
import com.orene.orenekaraoke.models.ItemOrder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by jati on 9/9/17.
 */

public class AdapterHistory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemOrder> listOrder;
    private Context context;

    public AdapterHistory(List<ItemOrder> listOrder, Context context) {
        this.listOrder = listOrder;
        this.context = context;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvAddress, tvDate;
        private ImageView ivThumbnails;
        private RelativeLayout containeritemOrder;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvDate = itemView.findViewById(R.id.tvDate);
            ivThumbnails = itemView.findViewById(R.id.ivThumbnails);
            containeritemOrder = itemView.findViewById(R.id.containerItemOrder);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ItemOrder itemOrder = listOrder.get(position);
        ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.tvName.setText(itemOrder.strNamePlace);
        viewHolder.tvAddress.setText(itemOrder.strAddress);
        viewHolder.tvDate.setText(itemOrder.strDate);
        Glide.with(context).load(Config.BASE_IMG_PLACE+itemOrder.thumbnails).into(viewHolder.ivThumbnails);
        viewHolder.containeritemOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_history_detail, null);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                TextView tvNamePlace = dialogView.findViewById(R.id.tv_nameplace);
                TextView tvRoom = dialogView.findViewById(R.id.tv_room);
                TextView tvStart = dialogView.findViewById(R.id.tv_start);
                TextView tvEnd = dialogView.findViewById(R.id.tv_end);
                TextView tvPrices = dialogView.findViewById(R.id.tv_price);
                tvNamePlace.setText(itemOrder.strNamePlace);
                tvRoom.setText(itemOrder.strNameType+" "+itemOrder.noRoom);
                tvStart.setText(itemOrder.strStartHour);
                tvEnd.setText(itemOrder.strEndHour);
                tvPrices.setText(itemOrder.strPriceType);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listOrder.size();
    }
}
