package com.orene.orenekaraoke.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.orene.orenekaraoke.R;
import com.orene.orenekaraoke.models.ItemRoom;

import java.util.List;

/**
 * Created by SYARIF on 20-Aug-17.
 */

public class SpinnerAdapter extends ArrayAdapter<ItemRoom> {
    private LayoutInflater layoutInflater;
    private Context context;
    private List<ItemRoom> data;

    public SpinnerAdapter(Context context, int resourceId, List<ItemRoom> data) {

        super(context, resourceId,data);
        layoutInflater = layoutInflater.from(context);
        this.data = data;
    }
    private View getCustomView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_spinner,parent,false);
        }
        ItemRoom current = data.get(position);
        TextView nama = convertView.findViewById(R.id.nama);
        nama.setText(current.strType);
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position,convertView,parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }
}
