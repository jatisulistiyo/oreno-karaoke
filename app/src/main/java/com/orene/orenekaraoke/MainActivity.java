package com.orene.orenekaraoke;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Helpers.BaseActivity;
import com.orene.orenekaraoke.Helpers.SessionManager;
import com.orene.orenekaraoke.fragments.FragmentHome;
import com.orene.orenekaraoke.fragments.FragmentHistory;
import com.orene.orenekaraoke.fragments.FragmentProfile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends BaseActivity {

    BottomNavigationView bottomNavigationView;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private HashMap<String,String> user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, new FragmentHome());
        fragmentTransaction.commit();

        fragmentManager = getSupportFragmentManager();

        bottomNavigationView = findViewById(R.id.bottomNavigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.menu_home:
                        fragment = new FragmentHome();
                        break;
                    case R.id.menu_order:
                        fragment = new FragmentHistory();
                        break;
                    case R.id.menu_profile:
                        fragment = new FragmentProfile();
                        break;
                }
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.frameLayout, fragment).commit();
                return true;
            }
        });
        user = sesi.getUserDetails();
        login();
    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        user = sesi.getUserDetails();
//
//    }
    private void login() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", user.get(SessionManager.KEY_EMAIL));
        params.put("password", user.get(SessionManager.KEY_PASS));
        aq.ajax(Config.BASE_URL + "home/login", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        if (object.getString("success").equals("1")) {
                            JSONArray jsonArray = object.getJSONArray("login");
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String email = jsonObject.getString("email");
                            String nama = jsonObject.getString("nama");
                            String cp = jsonObject.getString("cp");
                            String alamat = jsonObject.getString("alamat");
                            String orepay = jsonObject.getString("orepay");
                            String statusa = jsonObject.getString("status");
                            sesi.createLoginSession(email,user.get(SessionManager.KEY_PASS), nama, alamat, cp, orepay, statusa);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    login();
                                }
                            },5000);
                        }
                        else{
                            Toast.makeText(MainActivity.this,object.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    if(status.getCode() == 500){
                        Toast.makeText(c,"Server is busy or down. Try again!",Toast.LENGTH_SHORT).show();
                    }
                    else if(status.getCode() == 404){
                        Toast.makeText(c,"Resource not found!",Toast.LENGTH_SHORT).show();
                    }
                    else if(status.getCode() == -101){
                        Toast.makeText(c,"Check Internet Connection!",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(c,"Unexpected Error occured", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Logout");
            builder.setMessage("Are you sure to logout?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  sesi.logoutUser();
                }
            }).show();
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   dialog.dismiss();
                }
            }).show();
        }
        return super.onKeyDown(keyCode,event );
    }
}
