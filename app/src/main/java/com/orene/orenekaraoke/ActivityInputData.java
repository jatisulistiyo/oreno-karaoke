package com.orene.orenekaraoke;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Helpers.BaseActivity;
import com.orene.orenekaraoke.Helpers.ClickListener;
import com.orene.orenekaraoke.Helpers.RecyclerTouchListener;
import com.orene.orenekaraoke.Helpers.SessionManager;
import com.orene.orenekaraoke.adapters.AdapterRoom;
import com.orene.orenekaraoke.adapters.SpinnerAdapter;
import com.orene.orenekaraoke.models.ItemRoom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ActivityInputData extends BaseActivity {

    private Toolbar toolBar;
    EditText etDuration,etDate,etTime;
    TextView tvPrice,tvPlace;
    private RecyclerView recyclerView;
    List<ItemRoom> listItem = new ArrayList<>();
    List<ItemRoom> arrType = new ArrayList<>();
    AdapterRoom adapterRoom;
    Spinner spType;
    String strIdRoom,strIdType,strTime,strNameType, strIdPlace, numberpick="1", strPrice;
    int mYear,mMonth,mDay;
    Button btnCheck;
    HashMap<String,String> user;
    Calendar calendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        toolBar = findViewById(R.id.toolBar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle("Input Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spType = findViewById(R.id.sp_type);
        recyclerView = findViewById(R.id.rvRoom);
        etDuration = findViewById(R.id.et_duration);
        etDate = findViewById(R.id.et_date);
        etTime = findViewById(R.id.et_time);
        btnCheck = findViewById(R.id.btn_check);
        tvPrice = findViewById(R.id.tv_price);
        tvPlace = findViewById(R.id.tvPlace);
        tvPlace.setText(getIntent().getStringExtra("name_place"));
        user = sesi.getUserDetails();
        strIdPlace = getIntent().getStringExtra("id_place");
        getType();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(calendar.getTime());
        etDate.setText(formattedDate);
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityInputData.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String day;
                                if (dayOfMonth<10){
                                    day=year + "-" + (monthOfYear + 1) + "-" + "0"+String.valueOf(dayOfMonth);
                                }else{
                                    day = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                                }
                                etDate.setText(day);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        df = new SimpleDateFormat("HH:mm");
        formattedDate=df.format(calendar.getTime());
        strTime = formattedDate+":00";
        etTime.setText(strTime);
        etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(ActivityInputData.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        String strhour,strminute;
                        if (selectedHour<10){
                            strhour = "0"+String.valueOf(selectedHour);
                            if (selectedMinute<10){
                                strminute="0"+String.valueOf(selectedMinute);
                                strTime = strhour + ":" + strminute+":00";
                            }else{
                                strTime = strhour + ":" + selectedMinute + ":00";
                            }

                        }else {
                            if (selectedMinute<10){
                                strminute="0"+String.valueOf(selectedMinute);
                                strTime = selectedHour + ":" + strminute+":00";
                            }else{
                                strTime = selectedHour + ":" + selectedMinute + ":00";
                            }
                        }
                        etTime.setText( strTime);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        etDuration.setText("1");
        etDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final NumberPicker picker = new NumberPicker(getApplicationContext());
                picker.setMinValue(1);
                picker.setMaxValue(24);

                final FrameLayout layout = new FrameLayout(getApplicationContext());
                layout.addView(picker, new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        Gravity.CENTER));

                new AlertDialog.Builder(ActivityInputData.this)
                        .setView(layout)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                numberpick = String.valueOf(picker.getValue());
                                etDuration.setText(numberpick);
                                tvPrice.setText(String.valueOf(Integer.parseInt(numberpick)*Integer.parseInt(strPrice)));
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            }
        });
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRoom();
            }
        });
    }

    private void getRoom() {
        Map<String,String> params = new HashMap<>();
        params.put("id_type",strIdType);
        params.put("id_place", strIdPlace);
        params.put("start_datetime",etDate.getText().toString()+" "+etTime);
        params.put("duration",etDuration.getText().toString());
        aq.ajax(Config.BASE_URL+"home/list_room",params, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object!=null){
                    try {
                        if (object.getString("success").equals("1")){
                            listItem.clear();
                            JSONArray jsonArray = object.getJSONArray("room");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ItemRoom itemRoom = new ItemRoom();
                                itemRoom.strNumber = jsonObject.getString("no_room");
                                itemRoom.status = jsonObject.getString("status_room");
                                itemRoom.idRoom = jsonObject.getString("id_room");
                                listItem.add(itemRoom);
                            }
                            adapterRoom = new AdapterRoom(listItem,ActivityInputData.this);
                            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 5);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(adapterRoom);
                            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
                                @Override
                                public void onClick(View view, int position) {

                                    if (listItem.get(position).status.equals("Available")) {
                                        strIdRoom = listItem.get(position).idRoom;
                                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityInputData.this);
                                        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_order_detail, null);
                                        builder.setView(dialogView);
                                        final AlertDialog alertDialog = builder.create();
                                        Date date = null;
                                        String str = strTime;
                                        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                                        try {
                                            date = formatter.parse(str);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(date);
                                        calendar.add(Calendar.HOUR, Integer.parseInt(numberpick));
                                        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
                                        TextView tvNamePlace = dialogView.findViewById(R.id.tv_nameplace);
                                        TextView tvRoom = dialogView.findViewById(R.id.tv_room);
                                        TextView tvDate = dialogView.findViewById(R.id.tv_date);
                                        TextView tvTime = dialogView.findViewById(R.id.tv_time);
                                        final TextView tvPrices = dialogView.findViewById(R.id.tv_price);
                                        tvNamePlace.setText(getIntent().getStringExtra("name_place"));
                                        tvRoom.setText(strNameType+" "+listItem.get(position).strNumber);
                                        tvDate.setText(etDate.getText().toString());
                                        String strHour=String.valueOf(calendar.get(Calendar.HOUR)),strMinutes=String.valueOf(calendar.get(Calendar.MINUTE));
                                        if (Integer.parseInt(strHour)<10){
                                            strHour="0"+strHour;
                                        }
                                        if(Integer.parseInt(strMinutes)<10){
                                            strMinutes="0"+strMinutes;
                                        }
                                        tvTime.setText(etTime.getText().toString()+" - "+strHour+":"+strMinutes+":00");
                                        tvPrices.setText(tvPrice.getText().toString());

                                        btnCancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                alertDialog.dismiss();
                                            }
                                        });
                                        Button btnPay = dialogView.findViewById(R.id.btnPay);
                                        btnPay.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (Integer.parseInt(user.get(SessionManager.KEY_OREPAY))-Integer.parseInt(tvPrices.getText().toString())>=0) {
                                                    sendOrder();
                                                }else{
                                                    Toast.makeText(ActivityInputData.this, "Saldo Tidak Cukup", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });

                                        alertDialog.show();
                                    }else{
                                        Toast.makeText(c, "Room is Unavailable", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onLongClick(View view, int position) {

                                }
                            }));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void getType(){
        Map<String,String> params = new HashMap<>();
        params.put("id_place",strIdPlace);
        aq.ajax(Config.BASE_URL+"home/list_type",params, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object!=null){
                    try {
                        if (object.getString("success").equals("1")){
                            JSONArray jsonArray = object.getJSONArray("type");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ItemRoom itemRoom = new ItemRoom();
                                itemRoom.idType = jsonObject.getString("id_type");
                                itemRoom.strType = jsonObject.getString("name_type");
                                itemRoom.strPrice = jsonObject.getString("price_type");
                                arrType.add(itemRoom);
                            }
                            spType.setAdapter(new SpinnerAdapter(c,R.layout.list_spinner,arrType));
                            spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    strIdType = String.valueOf(arrType.get(i).idType);
                                    strNameType = String.valueOf(arrType.get(i).strType);
                                    strPrice = String.valueOf(arrType.get(i).strPrice);
                                    tvPrice.setText(String.valueOf(Integer.parseInt(strPrice)*Integer.parseInt(etDuration.getText().toString())));
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {
                                    strIdType = arrType.get(getIndex(arrType,String.valueOf(adapterView.getSelectedItem()))).idType;
                                    strPrice = arrType.get(getIndex(arrType,String.valueOf(adapterView.getSelectedItem()))).strPrice;
                                    strNameType = String.valueOf(adapterView.getSelectedItem());
                                    tvPrice.setText(String.valueOf(Integer.parseInt(strPrice)*Integer.parseInt(etDuration.getText().toString())));
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private void sendOrder(){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Logging in...");
        Map<String,String> params = new HashMap<>();
        params.put("id_room",strIdRoom);
        params.put("email",user.get(SessionManager.KEY_EMAIL));
        params.put("start_datetime",etDate.getText().toString()+" "+etTime.getText().toString());
        params.put("duration",etDuration.getText().toString());
        params.put("price",tvPrice.getText().toString());
        aq.progress(dialog).ajax(Config.BASE_URL+"home/send_order",params, JSONObject.class,new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object!=null){
                    try {
                        if (object.getString("success").equals("1")){
                            Intent i = new Intent(c,OrderFinishActivity.class);
                            i.putExtra("price",tvPrice.getText().toString());
                            startActivity(i);
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private int getIndex(List<ItemRoom> data,String id)
    {
        int index = 0;

        for (int i=0;i<data.size();i++){
            if (data.get(i).idType.equals(id)){
                index = i;
                break;
            }
        }
        return index;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
