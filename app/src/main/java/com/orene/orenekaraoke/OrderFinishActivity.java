package com.orene.orenekaraoke;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by jati on 9/10/17.
 */

public class OrderFinishActivity extends AppCompatActivity {
    TextView txtFinish;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_finish);
        txtFinish = findViewById(R.id.txt_finish);
        txtFinish.setText("You’ve pay "+getIntent().getStringExtra("price")+"; for karaoke in Happy Puppy ");
        Button btnDone = findViewById(R.id.btn_done);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
