package com.orene.orenekaraoke;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Helpers.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jati on 11/20/2017.
 */

public class FotgotPasswordActivity extends BaseActivity {
    EditText etEmail;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);
        etEmail= findViewById(R.id.et_email);
        Button btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validasiEmail(etEmail)){
                    forgot_send();
                }
            }
        });
    }
    public boolean validasiEmail(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        View focusview = null;

        if (!Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
            editText.setError(Html
                    .fromHtml("<font color='red'>Format email tidak sesuai (xxxx@yyy.zzz)</font>"));
            focusview = editText;
            focusview.requestFocus();
            return false;
        }

        return true;
    }
    private void forgot_send() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Sending Request...");
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", etEmail.getText().toString());
        aq.progress(dialog).ajax(Config.BASE_URL + "home/forgot_pass", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        Toast.makeText(FotgotPasswordActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        if (object.getString("success").equals("1")) {
                            finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    if (status.getCode() == 500) {
                        Toast.makeText(c, "Server is busy or down. Try again!", Toast.LENGTH_SHORT).show();
                    } else if (status.getCode() == 404) {
                        Toast.makeText(c, "Resource not found!", Toast.LENGTH_SHORT).show();
                    } else if (status.getCode() == -101) {
                        Toast.makeText(c, "Check Internet Connection!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(c, "Unexpected Error occured", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
