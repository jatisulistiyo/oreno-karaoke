package com.orene.orenekaraoke.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Config;
import com.orene.orenekaraoke.Helpers.SessionManager;
import com.orene.orenekaraoke.R;
import com.orene.orenekaraoke.adapters.AdapterHistory;
import com.orene.orenekaraoke.models.ItemOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jati on 9/9/17.
 */

public class FragmentHistory extends Fragment {

    private Toolbar toolBar;

    private List<ItemOrder> listPending = new ArrayList<>();
    private List<ItemOrder> listHistory = new ArrayList<>();
    private AdapterHistory adapterOrder;
    private RecyclerView rvPending,rvHistory;
    private AQuery aq;
    SessionManager sesi;
    HashMap<String,String> user;

    public FragmentHistory() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        toolBar = view.findViewById(R.id.toolBar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolBar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Order");
        sesi = new SessionManager(getActivity().getApplicationContext());
        user = sesi.getUserDetails();
        aq = new AQuery(getActivity().getApplicationContext());
        rvPending = view.findViewById(R.id.rv_pending);
        rvHistory = view.findViewById(R.id.rv_history);
       getData();
        return view;
    }

    private void getData() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", user.get(SessionManager.KEY_EMAIL));
        aq.ajax(Config.BASE_URL + "home/list_order", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        if (object.getString("success").equals("1")) {
                            JSONArray jsonArray = object.getJSONArray("list_pending");
                            for (int i=0;i<jsonArray.length();i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ItemOrder itemOrder = new ItemOrder();
                                String id_order = jsonObject.getString("id_order");
                                String name_place = jsonObject.getString("name_place");
                                String name_type = jsonObject.getString("name_type");
                                String no_room = jsonObject.getString("no_room");
                                String start_hour = jsonObject.getString("start_hour");
                                String end_hour = jsonObject.getString("end_hour");
                                String price_type = jsonObject.getString("price_type");
                                String date = jsonObject.getString("created_at");
                                String thumbnails = jsonObject.getString("url_img_place");
                                String address = jsonObject.getString("address_place");
                                String interval = jsonObject.getString("interval");
                                itemOrder.idOrder = id_order;
                                itemOrder.strNamePlace = name_place;
                                itemOrder.strNameType = name_type;
                                itemOrder.strAddress = address;
                                itemOrder.noRoom = no_room;
                                itemOrder.strStartHour = start_hour;
                                itemOrder.strEndHour = end_hour;
                                itemOrder.strPriceType = price_type;
                                itemOrder.strDate = date;
                                itemOrder.thumbnails = thumbnails;
                                itemOrder.interval = interval;
                                listPending.add(itemOrder);
                            }
                            adapterOrder = new AdapterHistory(listPending,getActivity());
                            rvPending.setLayoutManager(new LinearLayoutManager(getActivity()));
                            rvPending.setAdapter(adapterOrder);
                            jsonArray = object.getJSONArray("list_history");
                            for (int i=0;i<jsonArray.length();i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                ItemOrder itemOrder = new ItemOrder();
                                String id_order = jsonObject.getString("id_order");
                                String name_place = jsonObject.getString("name_place");
                                String name_type = jsonObject.getString("name_type");
                                String no_room = jsonObject.getString("no_room");
                                String start_hour = jsonObject.getString("start_hour");
                                String end_hour = jsonObject.getString("end_hour");
                                String price_type = jsonObject.getString("price_type");
                                String date = jsonObject.getString("created_at");
                                String thumbnails = jsonObject.getString("url_img_place");
                                String address = jsonObject.getString("address_place");
                                String interval = jsonObject.getString("interval");
                                itemOrder.idOrder = id_order;
                                itemOrder.strNamePlace = name_place;
                                itemOrder.strNameType = name_type;
                                itemOrder.strAddress = address;
                                itemOrder.noRoom = no_room;
                                itemOrder.strStartHour = start_hour;
                                itemOrder.strEndHour = end_hour;
                                itemOrder.strPriceType = price_type;
                                itemOrder.strDate = date;
                                itemOrder.thumbnails = thumbnails;
                                itemOrder.interval = interval;
                                listHistory.add(itemOrder);
                            }
                            adapterOrder = new AdapterHistory(listHistory,getActivity());
                            rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
                            rvHistory.setAdapter(adapterOrder);
                        }
                        else{
                            Toast.makeText(getActivity().getApplicationContext(),object.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    if(status.getCode() == 500){
                        Toast.makeText(getActivity(),"Server is busy or down. Try again!",Toast.LENGTH_SHORT).show();
                    }
                    else if(status.getCode() == 404){
                        Toast.makeText(getActivity(),"Resource not found!",Toast.LENGTH_SHORT).show();
                    }
                    else if(status.getCode() == -101){
                        Toast.makeText(getActivity(),"Check Internet Connection!",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getActivity(),"Unexpected Error occured", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
