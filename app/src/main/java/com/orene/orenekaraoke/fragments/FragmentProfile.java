package com.orene.orenekaraoke.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Config;
import com.orene.orenekaraoke.Helpers.SessionManager;
import com.orene.orenekaraoke.OrepayActivity;
import com.orene.orenekaraoke.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jati on 9/9/17.
 */

public class FragmentProfile extends Fragment {
    SessionManager sessionManager;
    EditText etPass, etCp, etAlamat;
    TextView tvName;
    TextView tvOrepay, tvTopUp;
    Button btnUpdate;
    AQuery aq;
    HashMap<String, String> user;

    public FragmentProfile() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        tvName = view.findViewById(R.id.tvName);
        etPass = view.findViewById(R.id.et_pass);
        etCp = view.findViewById(R.id.et_cp);
        etAlamat = view.findViewById(R.id.et_alamat);
        tvOrepay = view.findViewById(R.id.tv_orepay);
        btnUpdate = view.findViewById(R.id.btnUpdate);
        tvTopUp = view.findViewById(R.id.tv_topup);
        aq = new AQuery(getActivity().getApplicationContext());
        sessionManager = new SessionManager(getActivity().getApplicationContext());
        loadProfile();
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });
        tvTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), OrepayActivity.class);
                startActivity(i);
            }
        });
        return view;
    }

    private void loadProfile() {
        user = sessionManager.getUserDetails();
        tvName.setText(user.get(SessionManager.KEY_NAME));
        etPass.setText(user.get(SessionManager.KEY_PASS));
        etCp.setText(user.get(SessionManager.KEY_CP));
        etAlamat.setText(user.get(SessionManager.KEY_ALAMAT));
        tvOrepay.setText(user.get(SessionManager.KEY_OREPAY));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadProfile();
            }
        },5000);
    }

    private void update() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Logging in...");
        Map<String, String> params = new HashMap<String, String>();
        params.put("nama", tvName.getText().toString());
        params.put("password", etPass.getText().toString());
        params.put("cp", etCp.getText().toString());
        params.put("alamat", etAlamat.getText().toString());
        params.put("email", user.get(SessionManager.KEY_EMAIL));
        aq.progress(dialog).ajax(Config.BASE_URL + "home/update_profile", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        if (object.getString("success").equals("1")) {
                            JSONArray jsonArray = object.getJSONArray("login");
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String email = jsonObject.getString("email");
                            String nama = jsonObject.getString("nama");
                            String cp = jsonObject.getString("cp");
                            String alamat = jsonObject.getString("alamat");
                            String orepay = jsonObject.getString("orepay");
                            String statusa = jsonObject.getString("status");
                            sessionManager.createLoginSession(email, etPass.getText().toString(), nama, alamat, cp, orepay, statusa);
                            loadProfile();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
