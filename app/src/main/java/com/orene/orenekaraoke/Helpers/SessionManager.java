package com.orene.orenekaraoke.Helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.orene.orenekaraoke.LoginActivity;
import com.orene.orenekaraoke.MainActivity;

import java.util.HashMap;

/**
 * Created by SYARIF on 01-Jul-16.
 */
@SuppressLint("CommitPrefEdits")
public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "Sesi";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_NAME = "nama";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_ALAMAT = "alamat";
    public static final String KEY_CP = "cp";
    public static final String KEY_STATUS = "status";
    public static final String KEY_OREPAY = "orepay";
    public static final String KEY_PASS = "password";
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String email, String password, String nama, String alamat,String cp,String orepay, String status){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, nama);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_PASS,password);
        editor.putString(KEY_ALAMAT,alamat);
        editor.putString(KEY_CP,cp);
        editor.putString(KEY_OREPAY,orepay);
        editor.putString(KEY_STATUS,status);
        editor.commit();
    }


    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
            _context.startActivity(i);
            //((Activity)_context).finish();
        }
        else{
            Intent i = new Intent (_context,MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);

            _context.startActivity(i);
        }

    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_PASS, pref.getString(KEY_PASS, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_ALAMAT, pref.getString(KEY_ALAMAT, null));
        user.put(KEY_CP, pref.getString(KEY_CP,null));
        user.put(KEY_OREPAY,pref.getString(KEY_OREPAY,null));
        user.put(KEY_STATUS, pref.getString(KEY_STATUS, null));
        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(i);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
