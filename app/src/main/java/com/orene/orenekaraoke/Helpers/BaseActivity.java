package com.orene.orenekaraoke.Helpers;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AlphaAnimation;

import com.androidquery.AQuery;


public class BaseActivity extends AppCompatActivity{
    protected Context c;
    protected AQuery aq;
    protected SessionManager sesi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        c = this;
        aq = new AQuery(c);
        sesi = new SessionManager(c);
    }

}
