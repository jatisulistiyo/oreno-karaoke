package com.orene.orenekaraoke.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jati on 9/9/17.
 */

public class ItemRoom implements Parcelable {
    public String strNumber;
    public String strType;
    public String idType;
    public String status;
    public String idRoom;
    public String strPrice;

    public ItemRoom(){

    }
    public ItemRoom(String strNumber) {
        this.strNumber = strNumber;
    }


    protected ItemRoom(Parcel in) {
        strNumber = in.readString();
        strType = in.readString();
        idType = in.readString();
        status = in.readString();
        idRoom = in.readString();
        strPrice = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(strNumber);
        dest.writeString(strType);
        dest.writeString(idType);
        dest.writeString(status);
        dest.writeString(idRoom);
        dest.writeString(strPrice);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemRoom> CREATOR = new Creator<ItemRoom>() {
        @Override
        public ItemRoom createFromParcel(Parcel in) {
            return new ItemRoom(in);
        }

        @Override
        public ItemRoom[] newArray(int size) {
            return new ItemRoom[size];
        }
    };

    public String getStrNumber() {
        return strNumber;
    }
    public void setStrNumber(String strNumber) {
        this.strNumber = strNumber;
    }
}
