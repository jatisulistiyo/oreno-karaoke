package com.orene.orenekaraoke.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jati on 9/9/17.
 */

public class ItemOrder implements Parcelable {
    public String idOrder;
    public String strNamePlace;
    public String strAddress;
    public String strDate;
    public String strStartHour;
    public String strEndHour;
    public String noRoom;
    public String strPriceType;
    public String thumbnails;
    public String strNameType;
    public String interval;

    public ItemOrder(){

    }


    protected ItemOrder(Parcel in) {
        idOrder = in.readString();
        strNamePlace = in.readString();
        strAddress = in.readString();
        strDate = in.readString();
        strStartHour = in.readString();
        strEndHour = in.readString();
        noRoom = in.readString();
        strPriceType = in.readString();
        thumbnails = in.readString();
        strNameType = in.readString();
        interval = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idOrder);
        dest.writeString(strNamePlace);
        dest.writeString(strAddress);
        dest.writeString(strDate);
        dest.writeString(strStartHour);
        dest.writeString(strEndHour);
        dest.writeString(noRoom);
        dest.writeString(strPriceType);
        dest.writeString(thumbnails);
        dest.writeString(strNameType);
        dest.writeString(interval);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemOrder> CREATOR = new Creator<ItemOrder>() {
        @Override
        public ItemOrder createFromParcel(Parcel in) {
            return new ItemOrder(in);
        }

        @Override
        public ItemOrder[] newArray(int size) {
            return new ItemOrder[size];
        }
    };
}
