package com.orene.orenekaraoke.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jati on 9/9/17.
 */

public class ItemHome implements Parcelable {
    public String thumbnails;
    public String strName;
    public String strAddress;
    public String idPlace;
    public String Cp;

    public ItemHome(String thumbnails, String strName, String strAddress) {
        this.thumbnails = thumbnails;
        this.strName = strName;
        this.strAddress = strAddress;
    }
    public ItemHome(){

    }


    protected ItemHome(Parcel in) {
        thumbnails = in.readString();
        strName = in.readString();
        strAddress = in.readString();
        idPlace = in.readString();
        Cp = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbnails);
        dest.writeString(strName);
        dest.writeString(strAddress);
        dest.writeString(idPlace);
        dest.writeString(Cp);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemHome> CREATOR = new Creator<ItemHome>() {
        @Override
        public ItemHome createFromParcel(Parcel in) {
            return new ItemHome(in);
        }

        @Override
        public ItemHome[] newArray(int size) {
            return new ItemHome[size];
        }
    };

    public String getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(String thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }
}
