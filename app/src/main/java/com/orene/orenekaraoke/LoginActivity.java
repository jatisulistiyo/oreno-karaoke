package com.orene.orenekaraoke;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.orene.orenekaraoke.Helpers.BaseActivity;
import com.orene.orenekaraoke.Helpers.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jati on 10/22/2017.
 */

public class LoginActivity extends BaseActivity {
    EditText etEmail, etPass;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sessionManager = new SessionManager(getApplicationContext());
        etEmail = findViewById(R.id.et_email);
        etPass = findViewById(R.id.et_pass);

    }

    public void registerFunction(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void loginFunction(View view) {
        if (validasiEmail(etEmail) && validasi(etPass)) {
            login();
        }
    }

    public boolean validasi(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        View focusview = null;

        if (text.length() == 0) {
            editText.setError(Html
                    .fromHtml("<font color='red'>Tidak boleh kosong</font>"));
            focusview = editText;
            focusview.requestFocus();
            return false;
        }

        return true;
    }

    public boolean validasiEmail(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);
        View focusview = null;

        if (!Patterns.EMAIL_ADDRESS.matcher(text).matches()) {
            editText.setError(Html
                    .fromHtml("<font color='red'>Format email tidak sesuai (xxxx@yyy.zzz)</font>"));
            focusview = editText;
            focusview.requestFocus();
            return false;
        }

        return true;
    }

    private void login() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage("Sending Request...");
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", etEmail.getText().toString());
        params.put("password", etPass.getText().toString());
        aq.progress(dialog).ajax(Config.BASE_URL + "home/login", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                if (object != null) {
                    try {
                        if (object.getString("success").equals("1")) {
                            JSONArray jsonArray = object.getJSONArray("login");
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String email = jsonObject.getString("email");
                            String nama = jsonObject.getString("nama");
                            String cp = jsonObject.getString("cp");
                            String alamat = jsonObject.getString("alamat");
                            String orepay = jsonObject.getString("orepay");
                            String statusa = jsonObject.getString("status");
                            sessionManager.createLoginSession(email, etPass.getText().toString(), nama, alamat, cp, orepay, statusa);
                            sessionManager.checkLogin();
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    if (status.getCode() == 500) {
                        Toast.makeText(c, "Server is busy or down. Try again!", Toast.LENGTH_SHORT).show();
                    } else if (status.getCode() == 404) {
                        Toast.makeText(c, "Resource not found!", Toast.LENGTH_SHORT).show();
                    } else if (status.getCode() == -101) {
                        Toast.makeText(c, "Check Internet Connection!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(c, "Unexpected Error occured", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void ForgotClick(View view) {
        startActivity(new Intent(LoginActivity.this, FotgotPasswordActivity.class));
    }
}